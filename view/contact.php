<!DOCTYPE html>
<?php error_reporting(0)?>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <link  href="/public/css/style-page/contact.css" rel="stylesheet">
        <link  href="/public/css/style-template/footer-bryan.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <title>Contact</title>
    </head>
    <body>
    
        <?php include "template/navbar.php"?>
        

        <h1 id="titre" class="center">Contactez-nous</h1>

        <div id="sidebar">
            <div class="social_media">
                <a class="decorationoff" href="https://fr-fr.facebook.com/" target="_blank"><img src="/public/image/general/facebook.png" alt="logo facebook"></a>
                <a class="decorationoff" href="https://twitter.com/?lang=fr"target="_blank"><img src="/public/image/general/twitter.png" alt="logo twitter"></a>
                <a class="decorationoff" href="https://www.instagram.com/?hl=fr"target="_blank"><img src="/public/image/general/instagram.png" alt="logo instagram"></a>
            </div>
        </div>

        <div class="content">
            <div class="color_contact">
            <h2 class="title_contact">Appelez-nous: +33 1 23 45 67 89</h2>
                <h2 class="title_contact">Rencontrez-nous: 24 Avenue Daniel Rops, 73000 Chambéry</h2>
                <iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2792.016244763683!2d5.918455114943324!3d45.59021817910259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478ba87d244df4f5%3A0x8a23de29d967f2ae!2sLa%20Dynamo!5e0!3m2!1sfr!2sfr!4v1578481157371!5m2!1sfr!2sfr" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
            <form class="form_contact" action="contact.php" method="post" enctype="multipart/form-data" autocomplete="on">

                <div class="last_name">
                    <label for="last_name">Nom:</label>
                    <br>
                    <input class="user_info" type="text" name="user_last_name" placeholder="Votre nom" required="required">
                </div>
                <div class="first_name">
                    <label for="first_name">Prénom:</label>
                    <br>
                    <input class="user_info" type="text" name="user_first_name" placeholder="Votre prénom" required="required">
                </div>

                <div class="contact">
                    <label for="mail">Mail:</label>
                    <br>
                    <input class="user_info" type="email" name="user_mail" placeholder="Votre mail" required="required">
                    <br>
                    <label class="user_info" for="object">Sujet:</label>
                    <br>
                    <input class="user_info" type="text" name="user_object" placeholder="Sujet de la requet" required="required">
                </div>
                <div class="message">
                    <label for="message">Message:</label>
                    <br>
                    <textarea class="message_contact" name="message" rows="10" placeorder="message" required="required"></textarea>
                </div>
                <button class="send_button" type="submit" name="envoyer">Envoyer</button>
            </form>
        </div>

        <br><br>
        
        <?php include("template/footer-bryan.php");?>
        <?php include "link_contact.php"?>
        <script type="text/javascript" src="/public/js/library/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="/public/js/general.js"></script>
        <script type="text/javascript" src="/public/js/script-page/contact.js"></script>
    </body>
</html>


