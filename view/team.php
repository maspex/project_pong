<?php error_reporting(0)?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title> E-pong Team</title>
    <link rel="stylesheet" type="text/css" href="../public/css/style-page/team.css">
	<link rel="stylesheet" type="text/css" href="/public/css/style-template/footer-bryan.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
    <!--title of the page-->
<body>

<?php include "template/navbar.php"?>

		<div class="animation">
                <p>Bienvenue sur E-pong</p>
                <img src="../public/image/general/logo.svg">
            </div>
        <div class="titre">
        <h1 id="titre">NOS VALEURS</h1>
    </div>

    <div class="intro">
        <p>Notre motivation : vous permettre de passer des heures sympas à jouer à des jeux rétro. La base quoi !</p>
    </div>

    <!--1 containers with 2 cards by row (1 for the title, 1 for the photo, 1 for contact and 1 for the description)-->

    <div class="container">

        <div class="containercard">

				<?php
					$folder = scandir('../database');
					
                    foreach ($folder as $element) {
                        if (pathinfo($element, PATHINFO_EXTENSION) == 'db') {
                            $name_database = $element;
						}
					}
					
					$db=new PDO ('sqlite:../database/'. $name_database);
					$query=$db->query("SELECT * FROM Personnel");
					$result=$query->fetchAll();
				
				?>

            <!-- mon site devient dynamique car j'ai interrogé la base de données-->
			<?php foreach($result as $value): ?>
            <div class="carte">
                <h2 class="poste"><?= $value["job"] ?></h2>
                <img div class="image" src="<?= $value["image"] ?>" alt="photo testeur">
                <p class="coordonnees"><strong><span class=first_name> <?= $value["first_name"] ?> </span> <span class = last_name> <?= $value["last_name"] ?></span></strong><br><?= $value["mail"] ?> - <?= $value["tel"] ?></p>
                <p class="pres"><?= $value["presentation"] ?></p>
            </div>
			<?php endforeach; ?>

			<!--second card-->
			<!-- c'était statique avec 4 cartes et 4 codes-->
            <!-- 
				<div class="carte">
                <h2 class="poste">Testeur de jeux</h2>
                <p class="image"><img src="../public/image/membres/testeur.jpeg" alt="photo testeur"></p>
                <p class="coordonnees"><strong><span class=last_name> Tails </span> <span class = firt_name> Cristal</strong> <br>tails.cristal@e-pong.com - 06 12 34 56 78</p>
                <p class="pres">"Toujours prêt à l'action, à partir vers de nouveaux horizons à tout vitesse. Les jeux sont comme des joyaux qu'il faut protéger
                    et tester pour votre bonheur."</p>
            </div> -->
   
            <!--third card-->
            <!-- <div class="carte">
                <h2 class="poste">Gestionnaire de commandes</h2>
                <p class="image"><img src="../public/image/membres/testeur.jpeg" alt="photo testeur"></p>
                <p class="coordonnees"><strong><span class=last_name> Vania </span> <span class = firt_name> Castle</strong><br>vania.castle@e-pong.com - 06 12 34 56 78</p>
                <p class="pres">"Nulle commande ne me résiste parmi les trous de l'univers de notre entrepôt et l'adversité. Vos paquets seront gérés au petit soin depuis notre donjon."</p>
            </div> -->

            <!--fourth card-->
            <!-- <div class="carte">
                <h2 class="poste">Assistant administratif et comptable</h2>
                <p class="image"><img src="../public/image/membres/testeur.jpeg" alt="photo testeur"></p>
                <p class="coordonnees"><strong><span class=last_name> Axel </span> <span class = firt_name> Stone</strong><br>axel.stone@e-pong.com - 06 12 34 56 78</p>
                <p class="pres">"Rapidité de saisie, agilité dans les comptes, je suis le maître du jeu pour protéger vos données."</p>
            </div> -->
        </div>
    </div>
    
		</div>
		
		<?php include "template/footer-bryan.php"?>
		<script type="text/javascript" src="../public/js/library/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="../public/js/general.js"></script>
		<script type="text/javascript" src="../public/js/script-page/team.js"></script>
</body>	

</html>