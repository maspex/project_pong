<nav class="new-navbar">
    <div class="navbar-content">
        <a class="logo-container" href="/">
            <img src="/public/image/general/new_logo_white.png" class="logo-navabar" alt="logo E-pong">
        </a>
        <div class="links-container display-none">
            <div class="link-page">
                <a href="/view/product.php">Produits</a>
            </div>
            <div class="link-page">
                <a href="/view/news.php">Actualité</a>
            </div>
            <div class="link-page">
                <a href="/view/team.php">A propos de nous</a>
            </div>
            <div class="link-page">
                <a href="/view/contact.php">Nous contactez</a>
            </div>
        </div>
        <div class="icon-container">
            <a class="link-panier" href="#">
                <img src="/public/image/general/cart_white.svg" class="icon icon-panier" alt="icon panier">
            </a>
            <div class="menu-burger">
                <img src="/public/image/general/menu-burger.svg" class="icon icon-burger" alt="icon burger">
            </div>
        </div>
    </div>
</nav>

<style>

    @import url(http://weloveiconfonts.com/api/?family=entypo);
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);

    @media all and (max-width: 800px) {
        .links-container {
            display: block;
            position: absolute;
            z-index: 1000;
            right: 0;
            top: 70px;
            background-color: #2c3239;
        }

        .link-page {
            padding: 10px;
        }

        .menu-burger {
            display: block;
        }

        .logo-container {
            position: absolute;
            left: 0;
        }

        .display-none {
            display: none;
        }
    }

    @media all and (min-width: 800px) {
        .links-container {
            display: flex;
            height: 100%;
        }

        .menu-burger {
            display: none;
        }
    }
    body {
        margin: 0;
        padding-top: 70px;
    }
    .new-navbar {
        position: fixed;
        background: linear-gradient(36deg, rgba(2,0,36,1) 0%, rgba(47,23,72,1) 25%, rgba(35,124,16,1) 100%);
        min-width: 300px;
        width: 100%;
        height: 70px;
        top:0;
        z-index: 7;
    }

    .navbar-content {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100%;
    }

    .logo-navabar {
        margin: 5px 20px 5px 20px;
        width: 65px;
    }


    .link-page {
        display: flex;
        align-items: center;
        height: 100%;
    }

    .link-page:hover, .logo-container:hover {
        background-color: #fbfbfb30;
    }

    .link-page a {
        text-decoration: none;
        color: white;
        font-size: 1.2rem;
        margin-right: 10px;
        margin-left: 10px;
    }

    .icon-container {
        position: absolute;
        right: 0;
        display: flex;
        align-items: center;
        height: 100%;
        margin-left: 50px;
        padding: 0 10px 0 10px;
    }

    .icon-container .icon {
        width: 40px;
        margin-right: 25px;
    }
</style>