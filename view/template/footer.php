<div  class="flexborder">
    <div class="footer"">
        <img src="../public/image/general/logo.svg" alt="logo e-pong" width="50">E-pong

        <ul>
            <li class="navbar_contact2">
                <a class="navbar_contact" href="/view/product.php">Produits | </a>
            </li>
            <li class="navbar_contact2">
                <a class="navbar_contact" href="/view/news.php">Actualité | </a>
            </li>
            <li class="navbar_contact2">
                <a class="navbar_contact" href="/view/team.php">A propos de nous | </a>
            </li>
            <li class="navbar_contact2">
                <a class="navbar_contact" href="/view/contact.php">Contact</a>
            </li>
        </ul>
    </div>

    <div class="footer">
        <ul>
            <li class="style_none"><img class="image" src="../public/image/general/pin.png" alt="pin icon">24 Avenue Daniel Rops, 73000 Chambéry</li>
            <li class="style_none"><img class="image" src="../public/image/general/phone.png" alt="phone icon">+33 1 23 45 67 89</li>
            <li class="style_none"><img class="image" src="../public/image/general/mail.png" alt="mail icon"><a class="decoration_off" href="" target="_blank">coucou@jesuisunmail.com</a></li>
        </ul>
    </div>

    <div class="footer">
        <p>À propos:</p>
        <p>Notre motivation: vous permettre de passer des heures sympas à jouer à des jeux rétro. La base quoi !</p>

        <div class="center">
            <a href="https://fr-fr.facebook.com/" target="_blank"><img src="../public/image/general/facebook.png" alt="logo facebook"></a>
            <a href="https://twitter.com/?lang=fr"target="_blank"><img src="../public/image/general/twitter.png" alt="logo twitter"></a>
            <a href="https://www.instagram.com/?hl=fr"target="_blank"><img src="../public/image/general/instagram.png" alt="logo instagram"></a>
        </div>
    </div>
</div>