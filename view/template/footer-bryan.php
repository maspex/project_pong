<footer class="footer-bryan">
    <div class="wrapper">
        <div class="info info-logo-footer">
            <img src="/public/image/general/logo-white.svg" class="logo-svg-footer" alt="">
            <span class="text-logo-footer">E-Pong</span>
            <div class="links-page-footer">
                <a href="/view/product.php">Produit</a>
                <a href="/view/news.php">Actualités</a>
                <a href="/view/team.php">Equipe</a>
                <a href="#">Contact</a>
            </div>
            <p class="copyright">© 2020 E-Pong - Simplon - Promo 2</p>
        </div>

        <div class="info-coords-container">
            <div class="info">
                <h2>Nous contacter</h2>
                <div class="coord">
                    <img src="/public/image/general/marker.svg" alt="marker" class="svg-contact" />
                    <span>24 Avenue Daniel Rops, 73000 Chambéry</span>
                </div>

                <div class="coord">
                    <img src="/public/image/general/smartphone.svg" alt="smartphone" class="svg-contact" />
                    <span>06 ** ** ** **</span>
                </div>

                <div class="coord">
                    <img src="/public/image/general/mail.svg" alt="mail" class="svg-contact" />
                    <span>mail@mail.fr</span>
                </div>
                <div class="coord">
                    <a href="#">
                        <img src="/public/image/general/facebook.png" alt="" class="svg-reseaux fb" />
                    </a>
                    <a href="#">
                        <img src="/public/image/general/twitter.png" alt="" class="svg-reseaux twitter" />
                    </a>
                    <a href="#">
                        <img src="/public/image/general/instagram.png" alt="" class="svg-reseaux insta" />
                    </a>
                </div>
            </div>

        </div>

        <div class="info info-company">
            <h2>About the company</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore mag
                na aliqua. Ut enim ad minim veniam, quis nostrud exercit
                ation ullamco laboris nisi ut aliquip ex ea commodo cons
                equat. Duis aute irure dolor in reprehenderit in volupta
                te velit esse cillum dolore eu fugiat nulla pariatur. Ex
                cepteur sint occaecat cupidatat non proident, sunt in cu
                lpa qui officia deserunt mollit anim id est laborum.
            </p>
        </div>
    </div>
</footer>
