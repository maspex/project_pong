<?php
$path = $_SERVER['PHP_SELF'];
$FICHIER_COURANT = basename($path);
$NAME_PAGE_COURANT = substr($FICHIER_COURANT, 0, -4);

$RACINE_URL = $FICHIER_COURANT == "index.php" ? "" : "../";
$LINK_PAGE = $FICHIER_COURANT == "index.php" ? "view/" : "./";

$CSS_ACCUEIL_NAME = "accueil.css";
$URL_CSS_PAGE = $FICHIER_COURANT == "index.php" ? $CSS_ACCUEIL_NAME : $NAME_PAGE_COURANT . ".css";

$title_page = array(
            "Accueil" => "index.php",
            "Produits" => "product.php",
            "Actualités" => "news.php",
            "Contact" => "contact.php",
            "Equipe" => "team.php"

);
foreach ($title_page as $title_key => $file) {
    if ($FICHIER_COURANT == $file) {
        $title = "E-Pong - " . $title_key;
    }
}

?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link media="screen" href="/public/css/generale.css" rel="stylesheet">
<!--    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">-->
    <link media="screen" href="/public/css/style-template/nabvar.css" rel="stylesheet">
    <link media="screen" href="/public/css/style-template/footer-bryan.css" rel="stylesheet">
    <link media="screen" href="<?= "/public/css/style-page/" . $URL_CSS_PAGE?>"  rel="stylesheet">
    <title><?= $title?></title>
</head>
<body>