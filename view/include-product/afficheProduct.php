<li class="item">
    <div class="product">
        <div class="product-image-container">
            <a class="product-image"  title="<?= $product->getDescription()?>" href="/public/image/product/<?= $product->getImg()?>">
                <img src="/public/image/product/<?= $product->getImg()?>" alt="<?= $product->getName()?>"/>
            </a>
        </div>
        <hr>

        <div class="product-info">
            <div class="product-name-container">
                <p class="product-name"><?= $product->getName()?></p>
            </div>
            <div class="marque-container">
                <p class="marque"><?= $product->getMarque()?></p>
            </div>
            <div class="price-container">
                <p class="price"><?= $product->getPrice()?>€</p>
                <div class="rating">
                    <a title="Donner 5 étoiles">☆</a>
                    <a title="Donner 4 étoiles">☆</a>
                    <a title="Donner 3 étoiles">☆</a>
                    <a title="Donner 2 étoiles">☆</a>
                    <a title="Donner 1 étoile">☆</a>
                </div>
            </div>
        </div>
    </div>
    <hr>
</li>
