<?php
$products = array (
    [
        "id" => 1,
        "name" => "PS1",
        "description" => "Première console de Sony",
        "marque" => "Sony",
        "price" => 110,
        "img" => "ps1.png"
    ],
    [
        "id" => 2,
        "name" => "Pippin",
        "description" => "Première console d'apple",
        "marque" => "Apple",
        "price" => 60,
        "img" => "pippin.png"
    ],
    [
        "id" => 3,
        "name" => "Nintendo 64",
        "description" => "Console mytique de Nintendo",
        "marque" => "Nintendo",
        "price" => 90,
        "img" => "nintendo64.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],
    [
        "id" => 0,
        "name" => "Product",
        "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        "marque" => "Autre",
        "price" => 0,
        "img" => "controller-white.png"
    ],

);