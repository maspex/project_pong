<form class="menu-trie" action="" method="post">
    <div class="title-filtre">
        <h2>FILTRE</h2>
    </div>

    <div class="categories">
        <h2 class="titre-trie">Marque</h2>

        <label for="Nintendo">
            <input type="checkbox" name="Nintendo" <?=
            $checked = isset($_POST['Nintendo']) ? "checked" : null;
            ?>>
            <span>Nintendo</span>
        </label><br>

        <label for="Sony">
            <input type="checkbox" name="Sony" <?=
            $checked = isset($_POST['Sony']) ? "checked" : null;
            ?>>
            <span>Sony</span>
        </label><br>

        <label for="Apple">
            <input type="checkbox" name="Apple" <?=
            $checked = isset($_POST['Apple']) ? "checked" : null;
            ?>>
            <span>Apple</span>
        </label><br>

        <label for="Autre">
            <input type="checkbox" name="Autre" <?=
            $checked = isset($_POST['Autre']) ? "checked" : null;
            ?>>
            <span>Autre</span>
        </label><br>
        <button type="submit">Filtrer</button>
        <hr class="separateur">
    </div>

</form>
