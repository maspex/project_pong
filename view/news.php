<?php error_reporting(0)?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title> E-pong Actualités</title>
	<link rel="stylesheet" type="text/css" href="../public/css/style-page/news.css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="main">
		<header>
			<?php
			include "./template/navbar.php"; 
			?>
		</header>
		<hr style="color: black;">
		<section>
			<div class="animation">
				<p>Bienvenue sur E-pong</p>
				<img src="../public/image/general/logo.svg">
			</div>
			
			<div class="main_section">
				<div class="container">
				<?php
                $folder = scandir('../database');
                foreach ($folder as $element) {
                    if (pathinfo($element, PATHINFO_EXTENSION) == 'db') {
                        $name_database = $element;
                    }
                }
			try
			{
			$bdd = new PDO('sqlite:../database/'.$name_database);
			}
			catch (Exception $e)
			{
//				die('Erreur : ' . $e->getMessage());
			}
			$reponse = $bdd->query('SELECT * FROM news');
			while ($donnees = $reponse->fetch())
			{
			?>
					<div class="wrap">
						<div class="front">
						<img src="https://dribbble.s3.amazonaws.com/users/142298/screenshots/1194054/inkling-library-list-view7_1x.png" alt="image article" />
						<p class="entypo-mobile"><?php echo $donnees['titre']; ?></p>
						</div>
						<div class="back">
						<h1 class="entypo-mobile"><?php echo $donnees['titre']; ?></h1>
						<p><?php echo $donnees['content']; ?></p>
						<a href="#""><img src="../public/image/news/news.png" alt="logo cliquable">Read more...</a>
						</div>
					</div>	
			<?php
			}
			$reponse->closeCursor();
			?>  
		</section>
		<hr style="color: black;">
		<footer>
			<div class="main_footer_news">
				<fieldset>
					<legend>Footer</legend>
						<img src="https://img.icons8.com/material-sharp/24/000000/important-mail.png"><a href="mailto:simplon.truc@caramail.com">simplon.truc@caramail.com</a></li>
						<div class="telephone">
							<img src="https://img.icons8.com/material-sharp/24/000000/phone-office.png"><p>04 19 96 96 96</p>
						</div>
					<ul>
						<li>
						<a href="#">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<i class ="fa fa-facebook" aria-hidden="true"></i>
						</a>
						</li>
						<li>
						<a href="#">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<i class ="fa fa-google-plus" aria-hidden="true"></i>
						</a>
						</li>
						<li>
						<a href="#">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<i class ="fa fa-instagram" aria-hidden="true"></i>
						</a>
						</li>
						<li>
						<a href="#">
							<span></span>
							<span></span>
							<span></span>
							<span></span>
							<i class ="fa fa-linkedin" aria-hidden="true"></i>
						</a>
						</li>
					</ul>

					<img src="https://img.icons8.com/dotty/80/000000/space-fighter.png" class="space">
				</fieldset>
			</footer>
			<p class ="end">© 2020 E-Pong - Simplon - Promo 2</p>
		</div>
		<script src="../public/js/library/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="../public/js/general.js"></script>
		<script type="text/javascript" src="../public/js/script-page/news.js"></script>
</body>	
</html>
