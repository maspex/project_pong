<?php
error_reporting(0);
require "../class/Product.php";
$products = new Product();

include "./template/haut-de-page.php";
include "./template/navbar.php";
?>
    <h1 class="headline load-hidden">NOS PRODUITS</h1>
    <div class="main-container">
            <div class="main">
                <?php include "include-product/afficheMenuTrie.php" ?>
                <ul class="products-list">
                    <?php
                    $productsList = $products->getProducts();
                        foreach ($productsList as $key => $product) {
                            if (empty($_POST)) {
                                include "include-product/afficheProduct.php";
                            } else {
                                foreach ($_POST as $keyPost => $post) {
                                    if ($product->getMarque() == $keyPost) {
                                        include "include-product/afficheProduct.php";
                                    }
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
    </div>
<?php
include "./template/footer-bryan.php";
include "./template/bas-de-page.php";
?>
