<?php


class Model
{
    public function connectDatabase() {
        $folder = scandir('../database');
        foreach ($folder as $element) {
            if (pathinfo($element, PATHINFO_EXTENSION) == 'db') {
                $name_database = $element;
            }
        }

        try {
//            $db = new PDO('mysql:dbname=bdd;host=localhost:3308','root', '');
            $db = new PDO('sqlite:../database/'. $name_database);
        }

        catch(PDOException $e){
//            echo 'Connexion échouée : ' . $e->getMessage();
        }

        return $db;
    }
}