CREATE TABLE products (id  INTEGER PRIMARY KEY NOT NULL, name VARCHAR(255), description VARCHAR(255), marque VARCHAR(255), price INT, img VARCHAR(255));
CREATE TABLE news (id  INTEGER PRIMARY KEY NOT NULL, titre VARCHAR(255), content VARCHAR(255));
CREATE TABLE Contact (id  INTEGER PRIMARY KEY NOT NULL, created_date TEXT(255), first_name TEXT(255), last_name TEXT(255), email TEXT(255), sujet TEXT(255), messages TEXT(255));
CREATE TABLE Personnel ( id INTEGER PRIMARY KEY AUTOINCREMENT, last_name TEXT NOT NULL DEFAULT'', first_name TEXT NOT NULL DEFAULT '', job TEXT NOT NULL DEFAULT '', mail TEXT NOT NULL DEFAULT '', tel TEXT NOT NULL DEFAULT '', presentation TEXT NOT NULL DEFAULT '', image TEXT NOT NULL DEFAULT '');

INSERT INTO products VALUES
 (1, 'Nintendo 64',
 'La Nintendo 64 - également connue sous les noms de code Project Reality et Ultra 64 lors de sa phase de développement - est une console de jeux vidéo, sortie en 1996 (1997 en Europe), du constructeur nippon Nintendo en collaboration avec Silicon Graphics.',
 'Nintendo',
 150,
 'nintendo64.png'),
 (2,
 'Playstation 1',
 'La PlayStation 1 est une console de jeux vidéo de cinquième génération, conçue en 1994 par Sony Computer Entertainment, filiale entièrement dédiée à l’industrie du jeu vidéo du groupe japonais Sony Corporation.',
 'Sony',
 80,
 'ps1.png'
 ),
 (3, 'Pippin', 'La Pipp!n ou Pippin est une console de jeux vidéo conçue par Apple et commercialisée en 19962 par Bandai.',
 'Apple',
 0,
 'pippin.png'),
 (4, 'productDefault', 'Petite description de produit',
 'Autre',
 0,
 'controller-white.png'),
 (5, 'productDefault', 'Petite description de produit',
 'Autre',
 0,
 'controller-white.png'),
 (6, 'productDefault', 'Petite description de produit',
 'Autre',
 0,
 'controller-white.png'),
 (7, 'productDefault', 'Petite description de produit',
 'Autre',
 0,
 'controller-white.png'),
 (8, 'productDefault', 'Petite description de produit',
 'Autre',
 0,
 'controller-white.png');

INSERT INTO news (id,titre,content) VALUES (0,'titre 1','Kaamelott');
INSERT INTO news (id,titre,content) VALUES (1,'titre 2','Game of thrones');
INSERT INTO news (id,titre,content) VALUES (2,'titre 3','You');
INSERT INTO news (id,titre,content) VALUES (3,'titre 4','The Witcher');
INSERT INTO news (id,titre,content) VALUES (4,'titre 5','Ma bite');

INSERT INTO Personnel VALUES (1, 'Fighter','Ryu','Directeur et service après-vente','ruy.fighter@e-pong.com','06 12 34 56 78','Je vous accueille de manière punchy. Je suis dynamique et toujours prêt à vous apporter mon aide.','../public/image/membres/testeur.jpeg');
INSERT INTO Personnel VALUES (2 , 'Cristal','Tails','Testeur de jeux','tails.cristal@e-pong.com','06 12 34 56 78',"Toujours prêt à l'action, à partir vers de nouveaux horizons à tout vitesse. Les jeux sont comme des joyaux qu il faut protéger et tester pour votre bonheur.",'../public/image/membres/testeur.jpeg');
INSERT INTO Personnel VALUES (3, 'Castle','Vania','Gestionnaire de commandes','vania.castle@e-pong.com','06 12 34 56 78','Nulle commande ne me résiste parmi les trous de l univers de notre entrepôt et l adversité. Vos paquets seront gérés au petit soin depuis notre donjon.','../public/image/membres/testeur.jpeg');
INSERT INTO Personnel VALUES (4, 'Stone','Axel','Assistant administratif et comptable','axel.stone@e-pong.com','06 12 34 56 78','Rapidité de saisie, agilité dans les comptes, je suis le maître du jeu pour protéger vos données.','../public/image/membres/testeur.jpeg');
