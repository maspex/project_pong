# E-PONG - RETOUR VERS LE GAMING !

## QUI SOMMES NOUS ?
E-PONG est un site WEB de vente en ligne de produits **retro gaming** entre particuliers.

## ARCHITECTURE DOSSIER : 
* **CLASS** : Toutes les classes du projet
* **DATABASE** : SQL et fichier .db
* **PUBLIC** : JS, CSS, images ET polices
* **VIEW** : Nos pages PHP

## METTRE EN PLACE LA BASE DE DONNEES
* **ETAPE 1** : Créer un fichier **.db** dans la dossier **DATABASE** au même niveau que le dossier **MIGRATION**.
* **ETAPE 2** : Créer une nouvelle connexion sur **DBeaver** avec le fichier .db créé.
* **ETAPE 3**: 
Appliquer les **requêtes sql** du fichiers **22-01-2020_create-database-epong.sql** se trouvant dans le dossier **DATABASE/MIGRATION** 
à votre base de données.

* **ETAPE 4**: Ouvrez votre terminal au niveau du dossier du projet **E-PONG** et appliquez cette commande
``php -S localhost:8000``

Enfin allez à l'adresse http://localhost:8000.

ET voilà !! 
Vous pouvez maintenant découvrir note site web.
  
  