<?php
include "view/template/haut-de-page.php";
include "view/template/navbar.php";
?>
<div class="main-container">
    <div class="main">
        <section class="header-image">
            <img src="public/image/general/bandeau-accueil.png" alt="">
        </section>
        <section>
            <h1>ACCUEIL - E-PONG</h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                sed do eiusmod tempor incididunt ut labore et dolore mag
                na aliqua. Ut enim ad minim veniam, quis nostrud exercit
                ation ullamco laboris nisi ut aliquip ex ea commodo cons
                equat. Duis aute irure dolor in reprehenderit in volupta
                te velit esse cillum dolore eu fugiat nulla pariatur. Ex
                cepteur sint occaecat cupidatat non proident, sunt in cu
                lpa qui officia deserunt mollit anim id est laborum.
            </p>
        </section>
    </div>
</div>
<?php
include "view/template/footer-bryan.php";
include "view/template/bas-de-page.php";

?>

